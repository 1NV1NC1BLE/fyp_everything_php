<?php

namespace App\Model\M_Project;

use Illuminate\Database\Eloquent\Model;

class MProjectSubscription extends Model
{
    protected $table = 'MProjectSubscription';

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->MPS_DateSubscribe = $model->freshTimestamp();
        });
    }

    protected $fillable = [
        'PU_ID_FK', 'MP_ID_FK',
    ];
}
