<?php

namespace App\Model\M_Project;

use Illuminate\Database\Eloquent\Model;

class ProjectFeedback extends Model
{
    protected $table = 'ProjectFeedback';

    protected $primaryKey = 'PF_ID_PK';

    const CREATED_AT = 'PF_DateCreated';
    const UPDATED_AT = 'PF_DateUpdated';

    protected $fillable = [
        'PF_Title', 'PF_Message', 'PF_Rating', 'PU_ID_FK', 'MP_ID_FK'
    ];
}
