<?php

namespace App\Model\M_Project;

use Illuminate\Database\Eloquent\Model;

class ProjectProgress extends Model
{
    protected $table = 'ProjectProgress';

    protected $primaryKey = 'PP_ID_PK';

    const CREATED_AT = 'PP_DateCreated';
    const UPDATED_AT = 'PP_DateUpdated';

    protected $fillable = [
        'PP_Title', 'PP_Message', 'MP_ID_FK'
    ];
}
