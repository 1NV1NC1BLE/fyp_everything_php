<?php

namespace App\Model\M_Project;

use Illuminate\Database\Eloquent\Model;

class MaintenanceProject extends Model
{
    protected $table = 'MaintenanceProject';

    protected $primaryKey = 'MP_ID_PK';

    const CREATED_AT = 'MP_DateCreated';
    const UPDATED_AT = 'MP_DateUpdated';

}
