<?php

namespace App\Model\M_Project;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
    protected $table = 'ProjectStatus';

    protected $primaryKey = 'PS_ID_PK';

    public $timestamps = false;

}
