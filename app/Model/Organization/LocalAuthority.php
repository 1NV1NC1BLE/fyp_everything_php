<?php

namespace App\Model\Organization;

use Illuminate\Database\Eloquent\Model;

class LocalAuthority extends Model
{
    protected $table = 'LocalAuthority';

    protected $primaryKey = 'LA_ID_PK';

    public $timestamps = false;
}
