<?php
/**
 * Created by PhpStorm.
 * User: JUN2
 * Date: 06/05/2018
 * Time: 9:57 PM
 */

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class LAStaff extends Model
{
    protected $table = 'LAStaff';
    protected $primaryKey = 'LAS_ID_PK';
    public $timestamps = false;

}