<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'Role';
    protected $primaryKey = 'Ro_ID_PK';
    public $timestamps = false;

    //Eloquent Relationship Functions

    //user is replaced as people to avoid confusion
    public function people()
    {
        return $this->hasMany('App\Model\User\User', 'Ro_ID_FK', 'Ro_ID_PK');
    }
}
