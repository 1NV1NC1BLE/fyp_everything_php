<?php

namespace App\Model\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'User_ID_PK';
    public const CREATED_AT = 'User_DateCreated';
    public const UPDATED_AT = 'User_DateUpdated';

    protected $fillable = [
        'email', 'password', 'User_FirstName', 'User_LastName', 'Ro_ID_FK'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    //Eloquent Relationship Functions

    public function role()
    {
        return $this->belongsTo('App\Model\User\Role', 'Ro_ID_FK', 'Ro_ID_PK');
    }
}
