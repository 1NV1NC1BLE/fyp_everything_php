<?php
/**
 * Created by PhpStorm.
 * User: JUN2
 * Date: 06/05/2018
 * Time: 9:57 PM
 */

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class PublicUser extends Model
{
    protected $table = 'PublicUser';
    protected $primaryKey = 'PU_ID_PK';
    public $timestamps = false;

    protected $fillable = [
        'PU_ID_PK',
        'PU_DOB',
        'PU_ContactNo'
    ];


}