<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    protected $table = 'TicketStatus';

    protected $primaryKey = 'TS_ID_PK';

    public $timestamps = false;

}
