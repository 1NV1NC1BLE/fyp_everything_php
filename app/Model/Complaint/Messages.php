<?php

namespace App\Model\Complaint;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $table = 'Messages';

    protected $primaryKey = 'Mes_ID_PK';

    const CREATED_AT = 'Mes_DateCreated';
    const UPDATED_AT = 'Mes_DateUpdated';

    protected $fillable = [
        'Mes_Title', 'Mes_Message'
    ];
}
