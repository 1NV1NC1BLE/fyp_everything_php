<?php

namespace App\Model\Complaint;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    protected $table = 'FacilityType';

    protected $primaryKey = 'FT_ID_PK';

    public $timestamps = false;
}
