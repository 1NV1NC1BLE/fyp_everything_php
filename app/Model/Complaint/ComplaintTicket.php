<?php

namespace App\Model\Complaint;

use Illuminate\Database\Eloquent\Model;

class ComplaintTicket extends Model
{
    protected $table = 'ComplaintTicket';

    protected $primaryKey = 'CT_ID_PK';

    const CREATED_AT = 'CT_DateCreated';
    const UPDATED_AT = 'CT_DateUpdated';

}
