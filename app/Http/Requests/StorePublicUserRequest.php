<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|bail|string|email|max:128|unique:users',
            'passwords.password' => 'min:6|required_with:passwords.password_confirmation|same:passwords.password_confirmation',
            'passwords.password_confirmation' => 'min:6',
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'dob' => 'required|date',
            'contact' => 'required|string|max:30',
        ];
    }

}
