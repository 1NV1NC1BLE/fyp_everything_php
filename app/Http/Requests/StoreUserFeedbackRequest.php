<?php
/**
 * Created by PhpStorm.
 * User: JUN2
 * Date: 5/27/2018
 * Time: 11:07 AM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreUserFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:128',
            'message' => 'required|string|max:255',
            'rating' => 'required|numeric|min:1|max:5',
            'projectID' => 'required|numeric',
        ];
    }
}