<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * METHOD: POST
     * Login function for public user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticatePublicUser(Request $request)
    {
        $this->validateLogin($request);

        $query = "SELECT * FROM Users INNER JOIN PublicUser ";
        $query = $query."ON Users.User_ID_PK = PublicUser.PU_ID_PK INNER JOIN Role ";
        $query = $query."ON Users.Ro_ID_FK = Role.Ro_ID_PK ";
        $query = $query."WHERE Users.email = ? ";

        $user = DB::select($query,[$request->email]);

        if($user){
            if ($this->attemptLogin($request)) {

                $user = $this->guard()->user();
                $user->generateToken();

                return response()->json([
                    'token' => $user->api_token,
                ], 200);

            }
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * METHOD: POST
     * Login function for local authority staff
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticateLAStaff(Request $request)
    {
        $this->validateLogin($request);

        $query = "SELECT * FROM Users INNER JOIN LAStaff ";
        $query = $query . "ON Users.User_ID_PK = LAStaff.LAS_ID_PK INNER JOIN Role ";
        $query = $query . "ON Users.Ro_ID_FK = Role.Ro_ID_PK ";
        $query = $query . "WHERE Users.email = ?";
        $user = DB::select($query, [$request->email]);

        if ($user) {
            if ($this->attemptLogin($request)) {
                $user = $this->guard()->user();
                $user->generateToken();

                return response()->json([
                    'token' => $user->api_token
                ], 200);
            }
        }

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * METHOD: GET
     * Auto Login (Remember Me) for all user type
     * @return \Illuminate\Http\JsonResponse
     */
    public function autoLogin()
    {
        $user = Auth::guard('api')->user();
        if ($user != null) {
            return response()->json(['message' => 'User Authenticated'], 200);
        } else {
            return response()->json(['message' => 'User Not Authenticated'], 401);
        }
    }

    /**
     * METHOD: POST
     * Logout function for all ype of user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->remember_token = null;
            $user->save();
        }

        return response()->json(['message' => 'User logged out.'], 200);
    }
}
