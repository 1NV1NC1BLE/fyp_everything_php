<?php

namespace App\Http\Controllers\Auth\Register;

use App\Http\Controllers\Controller;
use App\Model\User\PublicUser;
use App\Model\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\StorePublicUserRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rule = new StorePublicUserRequest;
        return Validator::make($data, $rule->rules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Model\User\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['passwords']['password']),
            'User_FirstName' => $data['firstName'],
            'User_LastName' => $data['lastName'],
            'User_DateUpdated' => now(),
            'User_DateCreated' => now(),
            'Ro_ID_FK' => 1,
        ]);

//        DB::table('Users')->insert(
//            [
//                'email' => $data['email'],
//                'password' => Hash::make($data['passwords']['password']),
//                'User_FirstName' => $data['firstName'],
//                'User_LastName' => $data['lastName'],
//                'User_DateUpdated' => now(),
//                'User_DateCreated' => now(),
//                'Ro_ID_FK' => 1,
//            ]
//        );

        PublicUser::create([
            'PU_ID_PK' => $user->User_ID_PK,
            'PU_DOB' => $data['dob'],
            'PU_ContactNo' => $data['contact'],
        ]);

//        DB::table('PublicUser')->insert(
//            [
//                'PU_ID_PK' => $user->User_ID_PK,
//                'PU_DOB' => $data['dob'],
//                'PU_ContactNo' => $data['contact'],
//            ]
//        );

        return $user;
    }

    /**
     * Return response to client for successful registration
     * @param $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function registered($request, $user)
    {
        $user->generateToken();

        return response()->json([
            'message' => 'You have registered successfully!',
            'token' => $user->api_token,

        ], 201);
    }

    /**
     * METHOD: POST
     * Validate Email during registration process, check for existed email address
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateEmail(Request $request)
    {
        $testEmail = $request->email;
        $query = "SELECT email FROM Users WHERE email = ?";
        $email = DB::select($query, [$testEmail]);

        if ($email == $testEmail) {

            return response()->json([
                'isExisted' => true,
            ], 404);

        } else {
            return response()->json([
                'isExisted' => false,
            ], 200);
        }

    }
}
