<?php

namespace App\Http\Controllers\Organization;

use App\Model\Organization\LocalAuthority;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LocalAuthorityController extends Controller
{
    /**
     * METHOD: GET
     * Display a listing of the local authority.
     * @return localAuthority[]
     */
    public function index()
    {
        $query = "SELECT `LA_ID_PK`, `LA_FullName`, `LA_Acronym`, ";
        $query = $query. "`LA_Address`, `LA_Website`, `LA_Email`, `LA_ContactNo` ";
        $query = $query. "FROM `LocalAuthority` ORDER BY LA_Acronym";

        return DB::select($query);
    }
}
