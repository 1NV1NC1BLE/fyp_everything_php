<?php

namespace App\Http\Controllers\Complaint;

use App\Model\Complaint\FacilityType;
use App\Http\Controllers\Controller;

class FacilityTypeController extends Controller
{
    /**
     * METHOD: GET
     * Display a listing of the resource.
     * @return FacilityType[]
     */
    public function index()
    {
        return FacilityType::all();
    }

}
