<?php

namespace App\Http\Controllers\Complaint;

use App\Model\TicketStatus;
use App\Http\Controllers\Controller;

class TicketStatusController extends Controller
{
    /**
     * METHOD: GET
     * Display a listing of the resource.
     * @return TicketStatus[]
     */
    public function index()
    {
        return TicketStatus::all();
    }

}
