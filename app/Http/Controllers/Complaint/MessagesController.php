<?php

namespace App\Http\Controllers\Complaint;

use App\Model\Complaint\Messages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{

    /**
     * METHOD: GET
     * Get a list of message by ticket ID (local authority, public user)
     * @param $ticketid
     * @return array
     */
    public function getMessagesByTicketID($ticketid)
    {
        $query = 'SELECT * FROM Messages WHERE CT_ID_FK = ? ORDER BY Mes_ID_PK DESC';
         return DB::select($query, [$ticketid]);
    }

    /**
     * METHOD: POST
     * Local Authority staff add new message
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Singapore');
        $message = new Messages();
        $message->Mes_Title = $request->title;
        $message->Mes_Message = $request->message;
        $message->CT_ID_FK = $request->ticketID;
        $message->save();


        return response()->json([
            'Message' => 'New Message Created'
        ],201);

    }

}
