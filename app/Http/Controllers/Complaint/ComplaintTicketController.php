<?php

namespace App\Http\Controllers\Complaint;

use App\Model\Complaint\Messages;
use App\Model\Complaint\ComplaintTicket;
use App\Model\User\LAStaff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ComplaintTicketController extends Controller
{
    /**
     * METHOD: GET
     * Get list of ticket that submitted by the public user
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function retrievePublicUserTicket()
    {
        $id = Auth::id();

        $query = 'SELECT * FROM ComplaintTicket CT INNER JOIN FacilityType FT ';
        $query = $query . 'ON CT.FT_ID_FK = FT.FT_ID_PK INNER JOIN TicketStatus TS ';
        $query = $query . 'ON CT.TS_ID_FK = TS.TS_ID_PK INNER JOIN LocalAuthority LA ';
        $query = $query . 'ON CT.LA_ID_FK = LA.LA_ID_PK WHERE PU_ID_FK = ?';

        $dataSet = DB::select($query, [$id]);

        if($dataSet != null)
        {
            return $dataSet;
        }
        else
        {
            return response()->json([
                'Message' => 'Content Not Found!',
            ], 404);
        }
    }

    /**
     * METHOD: POST
     * Create new complaint ticket record that lodged by the public user
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Singapore');
        $random = rand(1000, 9999);
        $date = date('ymdhms', time());

        $id = $date . $random;

        $ticket = new ComplaintTicket();
        $myData = json_decode($request['data']); //access as json object(json_decode false, by default)

        $ticket->CT_ID_PK = $id;
        $ticket->LA_ID_FK = $myData->localAuthority;
        $ticket->CT_Title = $myData->title;
        $ticket->CT_Description = $myData->description;
        $ticket->FT_ID_FK = $myData->facilityType;
        $ticket->PU_ID_FK = Auth::id();
        $ticket->TS_ID_FK = 1; //To indicate the ticket is being pending


        if ($myData->address != "")
            $ticket->CT_Address = $myData->address;
        else {
            $ticket->CT_XCor = $myData->xCor;
            $ticket->CT_YCor = $myData->yCor;
        }

        if ($request->hasFile('photo1')) {

            $path1 = $request->file('photo1')->store('public/ticket-photos');
            $ticket->CT_Photo1Path = substr($path1, 7);

        }
        if ($request->hasFile('photo2')) {

            $path2 = $request->file('photo2')->store('public/ticket-photos');
            $ticket->CT_Photo2Path = substr($path2, 7);

        }
        if ($request->hasFile('photo3')) {

            $path3 = $request->file('photo3')->store('public/ticket-photos');
            $ticket->CT_Photo3Path = substr($path3, 7);
        }

        $ticket->save();

        //auto reply ticket received message to user
        $message = new Messages();
        $message->Mes_Title = "Ticket Received!";
        $message->Mes_Message = "Your ticket will be reviewed by the local authority staff soon.";
        $message->CT_ID_FK = $id;
        $message->save();


        return response()->json([
            'message' => 'You have lodge a complaint ticket successfully!',

        ], 201);
    }


    //Local Authority Controller
    /**
     * METHOD: GET
     * Retrieve list of tickets that pending for the local authority staff to accept or reject
     * @return array
     */
    public function retrieveLocalAuthorityPendingTicket()
    {
        $id = Auth::id();
        $laStaff = LAStaff::find($id);

        $query = 'SELECT * FROM ComplaintTicket CT INNER JOIN FacilityType FT ';
        $query = $query . 'ON CT.FT_ID_FK = FT.FT_ID_PK INNER JOIN TicketStatus TS ';
        $query = $query . 'ON CT.TS_ID_FK = TS.TS_ID_PK INNER JOIN LocalAuthority LA ';
        $query = $query . 'ON CT.LA_ID_FK = LA.LA_ID_PK WHERE CT.LA_ID_FK = ? AND TS.TS_ID_PK = 1';

        $dataSet = DB::select($query, [$laStaff->LA_ID_FK]);

        return $dataSet;

    }
    /**
     * METHOD: GET
     * Get list of ticket that approved by the local authority staff
     * @return array
     */
    public function retrieveLocalAuthorityApprovedTicket()
    {
        $id = Auth::id();
        $laStaff = LAStaff::find($id);

        $query = 'SELECT * FROM ComplaintTicket CT INNER JOIN FacilityType FT ';
        $query = $query . 'ON CT.FT_ID_FK = FT.FT_ID_PK INNER JOIN TicketStatus TS ';
        $query = $query . 'ON CT.TS_ID_FK = TS.TS_ID_PK INNER JOIN LocalAuthority LA ';
        $query = $query . 'ON CT.LA_ID_FK = LA.LA_ID_PK WHERE CT.LA_ID_FK = ? AND (TS.TS_ID_PK = 3 OR TS.TS_ID_PK = 5) AND CT.LAS_ID_FK = ?';

        $dataSet = DB::select($query, [$laStaff->LA_ID_FK, $id]);

        return $dataSet;

    }

    /**
     * METHOD: GET
     * Get list of ticket that rejected by the local authority staff
     * @return array
     */
    public function retrieveLocalAuthorityRejectedTicket()
    {
        $id = Auth::id();
        $laStaff = LAStaff::find($id);

        $query = 'SELECT * FROM ComplaintTicket CT INNER JOIN FacilityType FT ';
        $query = $query . 'ON CT.FT_ID_FK = FT.FT_ID_PK INNER JOIN TicketStatus TS ';
        $query = $query . 'ON CT.TS_ID_FK = TS.TS_ID_PK INNER JOIN LocalAuthority LA ';
        $query = $query . 'ON CT.LA_ID_FK = LA.LA_ID_PK WHERE CT.LA_ID_FK = ? AND TS.TS_ID_PK = 4 AND CT.LAS_ID_FK = ?';

        $dataSet = DB::select($query, [$laStaff->LA_ID_FK, $id]);

        return $dataSet;
    }

    /**
     * METHOD: GET
     * Local Authority staff to approve a complaint ticket that submitted by the public user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveTicket($id)
    {
        date_default_timezone_set('Asia/Singapore');

        //Modify status and localAuthorityStaff id in complaint ticket
        $ticket = ComplaintTicket::find($id);
        $ticket->TS_ID_FK = 3;
        $ticket->LAS_ID_FK = Auth::id();
        $ticket->save();

        //auto reply ticket accepted message to user
        $message = new Messages();
        $message->Mes_Title = "Ticket Approved!";
        $message->Mes_Message = "Your ticket has been reviewed and accepted!\n You will be auto-subscribed to the project once the local authority has initiated the maintenance project";
        $message->CT_ID_FK = $id;
        $message->save();

        return response()->json([
            'Message' => 'Ticket '.$id.' has been accepted!',
        ], 200);
    }

    /**
     * METHOD: GET
     * Local Authority staff to reject a complaint ticket that submitted by the public user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectTicket($id)
    {
        date_default_timezone_set('Asia/Singapore');

        //Modify status and localAuthorityStaff id in complaint ticket
        $ticket = ComplaintTicket::find($id);
        $ticket->TS_ID_FK = 4;
        $ticket->LAS_ID_FK = Auth::id();
        $ticket->save();

        //auto reply ticket accepted message to user
        $message = new Messages();
        $message->Mes_Title = "Ticket Rejected!";
        $message->Mes_Message = "Your ticket has been rejected, the reason of rejection will be replied by the local authority staff soon!";
        $message->CT_ID_FK = $id;
        $message->save();

        return response()->json([
            'Message' => 'Ticket '.$id.' has been rejected!',
        ], 200);
    }
}
