<?php

namespace App\Http\Controllers\M_Project;

use App\Model\M_Project\MaintenanceProject;
use App\Model\M_Project\MProjectSubscription;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|The public user can get project subscription through the following condition:
|-Subscribe the project manually from Other Project Page (use the following function)
|                                   OR
|-Auto subscribe by the system a project is initiated by a local authority which is
|reported by the respective user (not through the following function)
|(Please check MaintenanceProjectController)
|
*/

class MProjectSubscriptionController extends Controller
{
    /**
     * METHOD: GET
     * Public User manually subscribe the project themselves
     * @param $projectID
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function publicUserSubscribeProject($projectID)
    {
        date_default_timezone_set('Asia/Singapore');

        $id = Auth::id();

        //Check to ensure only public user is entitled to subscribe the project
        $query = "SELECT U.User_ID_PK FROM Users U INNER JOIN Role R ";
        $query = $query . "ON U.Ro_ID_FK = R.Ro_ID_PK INNER JOIN PublicUser PU ";
        $query = $query . "ON U.User_ID_PK = PU.PU_ID_PK ";
        $query = $query . "WHERE R.Ro_Name = 'Public User' AND PU.PU_ID_PK = ?";

        if (DB::select($query, [$id]) == null) {
            return response([
                "Message" => "Access Denied!"
            ], 403);
        }
        //Validate Project ID
        $project = MaintenanceProject::find($projectID);
        if ($project == null) {
            return response([
                "Message" => "Maintenance Project Not Exist!"
            ], 404);
        }

        //Check if existing subscription is being subscribed by the user
        $querySub = "SELECT * FROM MProjectSubscription ";
        $querySub = $querySub . "WHERE PU_ID_FK = ? AND MP_ID_FK = ?";

        if (DB::select($querySub, [$id, $projectID]) != null) {
            return response([
                "Message" => "User is already subscribed to this project!"
            ], 400);
        }

        //Subscribe the project to the user.
        MProjectSubscription::create([
            "PU_ID_FK" => $id,
            "MP_ID_FK" => $projectID,

        ]);

        return response([
            "Message" => "User is successfully subscribed to the project!"
        ], 201);

    }
}
