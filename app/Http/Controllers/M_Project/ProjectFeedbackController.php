<?php

namespace App\Http\Controllers\M_Project;

use App\Http\Requests\StoreUserFeedbackRequest;
use App\Model\M_Project\ProjectFeedback;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProjectFeedbackController extends Controller
{
    /**
     * METHOD: GET
     * Retrieve list of feedback based on the provided Maintenance Project ID from the local authority staff
     * @param $projectid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFeedbackListForLAStaff($projectid)
    {

        $query = 'SELECT PF_ID_PK, PF_Title, PF_Message, PF_Rating, PF_DateCreated, PF_DateUpdated, ';
        $query = $query . 'PU.PU_ProfilePicPath, U.User_FirstName, U.User_LastName ';
        $query = $query . 'FROM ProjectFeedback PF INNER JOIN PublicUser PU ';
        $query = $query . 'ON PF.PU_ID_FK = PU_ID_PK INNER JOIN Users U ';
        $query = $query . 'ON U.User_ID_PK = PF.PU_ID_FK WHERE MP_ID_FK = ? ORDER BY PF_DateCreated DESC';

        $feedbackList = DB::select($query, [$projectid]);

        $queryOverallInfo = 'SELECT ROUND(AVG(PF_Rating),2) AS avgRating, COUNT(PF_ID_PK) AS noReview FROM ProjectFeedback WHERE MP_ID_FK = ?';

        $overallInfo = DB::select($queryOverallInfo, [$projectid]);

        return response()->json([
            'overallInfo' => $overallInfo,
            'userFeedbackList' => $feedbackList,

        ], 200);
    }


    /**
     * METHOD: GET
     * Retrieve list of feedback based on the provided Maintenance Project ID from the public user
     * @param $projectid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFeedbackListForPublicUser($projectid)
    {
        $userID = $id = Auth::id();

        $queryOtherUser = 'SELECT PF_ID_PK, PF_Title, PF_Message, PF_Rating, PF_DateCreated, PF_DateUpdated, ';
        $queryOtherUser = $queryOtherUser . 'PU.PU_ProfilePicPath, U.User_FirstName, U.User_LastName ';
        $queryOtherUser = $queryOtherUser . 'FROM ProjectFeedback PF INNER JOIN PublicUser PU ';
        $queryOtherUser = $queryOtherUser . 'ON PF.PU_ID_FK = PU_ID_PK INNER JOIN Users U ';
        $queryOtherUser = $queryOtherUser . 'ON U.User_ID_PK = PF.PU_ID_FK WHERE MP_ID_FK = ? AND PU_ID_FK <> ?';

        $otherFeedback = DB::select($queryOtherUser, [$projectid, $userID]);

        $queryUser = 'SELECT PF_ID_PK, PF_Title, PF_Message, PF_Rating, PF_DateCreated, PF_DateUpdated, ';
        $queryUser = $queryUser . 'PU.PU_ProfilePicPath, U.User_FirstName, U.User_LastName ';
        $queryUser = $queryUser . 'FROM ProjectFeedback PF INNER JOIN PublicUser PU ';
        $queryUser = $queryUser . 'ON PF.PU_ID_FK = PU_ID_PK INNER JOIN Users U ';
        $queryUser = $queryUser . 'ON U.User_ID_PK = PF.PU_ID_FK WHERE MP_ID_FK = ? AND PU_ID_FK = ?';

        $myFeedback = DB::select($queryUser, [$projectid, $userID]);

        $queryOverallInfo = 'SELECT ROUND(AVG(PF_Rating),2) AS avgRating, COUNT(PF_ID_PK) AS noReview FROM ProjectFeedback WHERE MP_ID_FK = ?';

        $overallInfo = DB::select($queryOverallInfo, [$projectid]);

        return response()->json([
            'overallInfo' => $overallInfo,
            'userFeedback' => $myFeedback,
            'otherUserFeedback' => $otherFeedback,

        ], 200);
    }

    /**
     * METHOD: POST
     * Store feedback submitted by Public User
     * @param StoreUserFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeFeedback(StoreUserFeedbackRequest $request)
    {
        date_default_timezone_set('Asia/Singapore');
        $id = Auth::id();

        ProjectFeedback::create([
            'PF_Title' => $request->title,
            'PF_Message' => $request->message,
            'PF_Rating' => $request->rating,
            'PU_ID_FK' => $id,
            'MP_ID_FK' => $request->projectID,
        ]);

        return response()->json([
            'Message' => 'Feedback Submitted!'
        ], 201);

    }

    /**
     * METHOD: PUT
     * Update Feedback submitted by Public User
     * @param StoreUserFeedbackRequest $request
     * @param $feedbackID
     * @return \Illuminate\Http\JsonResponse
     */
    public function editFeedback(StoreUserFeedbackRequest $request, $feedbackID)
    {
        date_default_timezone_set('Asia/Singapore');

        $feedback = ProjectFeedback::find($feedbackID);

        if ($feedback != null) {
            $feedback->PF_Title = $request->title;
            $feedback->PF_Message = $request->message;
            $feedback->PF_Rating = $request->rating;

            $feedback->save();

            return response()->json([
                'Message' => 'Feedback Record Updated!'
            ], 200);
        } else {
            return response()->json([
                'Message' => 'Feedback Record Not Found!'
            ], 404);
        }
    }

}
