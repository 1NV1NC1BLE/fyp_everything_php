<?php

namespace App\Http\Controllers\M_Project;

use App\Model\Complaint\ComplaintTicket;
use App\Model\M_Project\MaintenanceProject;
use App\Model\M_Project\MProjectSubscription;
use App\Model\M_Project\ProjectProgress;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MaintenanceProjectController extends Controller
{
    //Public User's Controller method
    /**
     * METHOD: GET
     * Retrieve list of projects that subscribed by the public user
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getSubscribedProject()
    {
        $id = Auth::id();

        $query = "SELECT * FROM MaintenanceProject MP LEFT JOIN ComplaintTicket CT ";
        $query = $query . "ON MP.CT_ID_FK = CT.CT_ID_PK INNER JOIN ProjectStatus PS ";
        $query = $query . "ON MP.PS_ID_FK = PS.PS_ID_PK INNER JOIN LAStaff LAS ";
        $query = $query . "ON MP.LAS_ID_FK = LAS.LAS_ID_PK INNER JOIN LocalAuthority LA ";
        $query = $query . "ON LAS.LA_ID_FK = LA.LA_ID_PK LEFT JOIN MProjectSubscription MPS ";
        $query = $query . 'ON MP.MP_ID_PK = MPS.MP_ID_FK WHERE MPS.PU_ID_FK = ?';

        $dataSet = DB::select($query, [$id]);

        if ($dataSet != null) {
            return $dataSet;
        } else {
            return response()->json([
                'Message' => 'Content Not Found!',
            ], 404);
        }
    }

    /**
     * METHOD: GET
     * Retrieve list of project list based on the provided selected local authority id
     * @param $laID
     * @return array
     */
    public function getOtherProject($laID)
    {
        $id = Auth::id();

        $query = "SELECT * FROM MaintenanceProject MP LEFT JOIN ComplaintTicket CT ";
        $query = $query . "ON MP.CT_ID_FK = CT.CT_ID_PK INNER JOIN ProjectStatus PS ";
        $query = $query . "ON MP.PS_ID_FK = PS.PS_ID_PK INNER JOIN LAStaff LAS ";
        $query = $query . "ON MP.LAS_ID_FK = LAS.LAS_ID_PK INNER JOIN LocalAuthority LA ";
        $query = $query . "ON LAS.LA_ID_FK = LA.LA_ID_PK LEFT JOIN MProjectSubscription MPS ";
        $query = $query . "ON MP.MP_ID_PK = MPS.MP_ID_FK ";
        $query = $query . "WHERE LA.LA_ID_PK = ? AND (MPS.PU_ID_FK IS NULL OR MPS.PU_ID_FK <> ?)";

        $otherProject = DB::select($query, [$laID, $id]);

        return $otherProject;

    }

    //Local Authority Staff's Controller method

    /**
     * METHOD: GET
     * Retrieve list of maintenance project requested by the public user
     * @return array
     */
    public function getPublicRequestProject()
    {
        $id = Auth::id();

        $query = "SELECT * FROM MaintenanceProject MP INNER JOIN ComplaintTicket CT ";
        $query = $query . "ON MP.CT_ID_FK = CT.CT_ID_PK INNER JOIN ProjectStatus PS ";
        $query = $query . "ON MP.PS_ID_FK = PS.PS_ID_PK INNER JOIN LocalAuthority LA ";
        $query = $query . "ON CT.LA_ID_FK = LA.LA_ID_PK ";
        $query = $query . "WHERE CT.LAS_ID_FK = ?";

        $otherProject = DB::select($query, [$id]);

        return $otherProject;
    }

    /**
     * METHOD: GET
     * Retrieve list of maintenance project (that self requested by local authority)
     * @return array
     */
    public function getLocalAuthorityStaffProject()
    {
        $id = Auth::id();

        $query = "SELECT * FROM MaintenanceProject MP INNER JOIN ProjectStatus PS ";
        $query = $query . "ON MP.PS_ID_FK = PS.PS_ID_PK INNER JOIN LAStaff LAS ";
        $query = $query . "ON MP.LAS_ID_FK = LAS.LAS_ID_PK INNER JOIN LocalAuthority LA ";
        $query = $query . "ON LAS.LA_ID_FK = LA.LA_ID_PK ";
        $query = $query . "WHERE MP.LAS_ID_FK = ? AND MP.CT_ID_FK IS NULL";

        $otherProject = DB::select($query, [$id]);

        return $otherProject;
    }

    /**
     * METHOD: POST
     * Create new maintenance project (Both Public Request / Local Authority Project)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createMaintenanceProject(Request $request)
    {
        date_default_timezone_set('Asia/Singapore');
        $random = rand(100, 999);
        $date = date('ymd', time());

        $id = $date . $random;

        $project = new MaintenanceProject();
        $project->MP_ID_PK = $id;
        $project->MP_Title = $request->title;
        $project->MP_Description = $request->description;
        $project->MP_EstimateBudget = $request->budget;
        $project->MP_StartDate = $request->startDate;
        $project->MP_EndDate = $request->endDate;
        $project->PS_ID_FK = 1;
        $project->LAS_ID_FK = Auth::id();

        if ($request->ticketID != null) {
            //Update status in ComplaintTicket to 'Project Created'
            $ticket = ComplaintTicket::find($request->ticketID);
            $ticket->TS_ID_FK = 5;
            $ticket->save();

            //references to ticket ID
            $project->CT_ID_FK = $request->ticketID;

            //Auto subscribe the user to the new created project
            $subscription = new MProjectSubscription();
            $subscription->PU_ID_FK = $ticket->PU_ID_FK;
            $subscription->MP_ID_FK = $project->MP_ID_PK;
            $subscription->save();
        }

        $project->save();

        if ($request->progressTitle != null && $request->progressDes != null) {
            $progress = new ProjectProgress();
            $progress->PP_Title = $request->progressTitle;
            $progress->PP_Message = $request->progressDes;
            $progress->MP_ID_FK = $id;
            $progress->save();
        }

        return response()->json([
            'Message' => 'New maintenance project created!',
        ], 201);
    }

    /**
     * METHOD: PUT
     * Update the specified maintenance project details
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMaintenanceProject(Request $request, $id)
    {
        date_default_timezone_set('Asia/Singapore');

        $project = MaintenanceProject::find($id);

        $project->MP_Title = $request->title;
        $project->MP_Description = $request->description;
        $project->MP_EstimateBudget = $request->budget;
        $project->MP_StartDate = $request->startDate;
        $project->MP_EndDate = $request->endDate;
        $project->PS_ID_FK = $request->status;

        $project->save();

        return response()->json([
            'Message' => 'Project Detail is updated!',
        ], 200);
    }

}
