<?php

namespace App\Http\Controllers\M_Project;

use App\Model\M_Project\ProjectProgress;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectProgressController extends Controller
{
    /**
     * METHOD: POST
     * Store Project Progress that submitted by local authority staff
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Singapore');
        ProjectProgress::create([
            'PP_Title' => $request->progress,
            'PP_Message' => $request->description,
            'MP_ID_FK' => $request->projectID
        ]);

        return response()->json([
            'Message' => 'New project progress successfully added!'
        ],200);
    }

    /**
     * METHOD: GET
     * Retrieve a list of project progress based on the project ID
     * @param $projectid
     * @return array
     */
    public function getProgress($projectid)
    {
        $query = "SELECT `PP_ID_PK`, `PP_Title`, `PP_Message`, `PP_DateCreated` ";
        $query = $query. 'FROM `ProjectProgress` WHERE `MP_ID_FK` = ? ORDER BY `PP_DateCreated` DESC';

        return DB::select($query, [$projectid]);
    }

}
