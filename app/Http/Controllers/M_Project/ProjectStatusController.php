<?php

namespace App\Http\Controllers\M_Project;

use App\Http\Controllers\Controller;
use App\Model\M_Project\ProjectStatus;

class ProjectStatusController extends Controller
{
    /**
     * METHOD: GET
     * Display a listing of the resource.
     * @return ProjectStatus[]
     */
    public function index()
    {
        return ProjectStatus::all();
    }

}
