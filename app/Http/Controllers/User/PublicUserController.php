<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Model\User\PublicUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PublicUserController extends Controller
{
    /**
     * METHOD: GET
     * Return Public User Account Info
     * @return array
     */
   public function getUserProfileInfo()
   {
       $id = Auth::id();

       $query = "SELECT U.email, U.User_FirstName, U.User_LastName, PU.PU_DOB, PU.PU_ContactNo, PU.PU_ProfilePicPath ";
       $query = $query."FROM Users U INNER JOIN PublicUser PU ";
       $query = $query."ON U.User_ID_PK = PU.PU_ID_PK WHERE U.User_ID_PK = ? ";

       $profileData = DB::select($query,[$id]);

       return $profileData;
   }

    /**
     * METHOD: POST
     * Change Public User Account Password
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function changeUserPassword(ChangePasswordRequest $request)
   {
       $user = Auth::guard('api')->user();

       if(Hash::check($request->currentPassword, $user->password))
       {
           $user->password = Hash::make($request->newPassword);
           $user->api_token = null;
           $user->save();

           return response()->json([
               'Message' => 'Password successfully changed, user is logged out!'], 200);
       }
       else
       {
           return response()->json([
               'Message' => 'Invalid Current Password!'], 400);
       }
   }

    /**
     * METHOD: POST
     * Change Public User Account Avatar (Profile Picture)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function changeProfilePic(Request $request)
   {
       $userID = Auth::id();

       if ($request->hasFile('profilePic'))
       {
           $path = $request->file('profilePic')->store('public/profile-pic');

           $publicUser = PublicUser::find($userID);

           if($publicUser->PU_ProfilePicPath != null)
           {
               //delete existing file
               Storage::delete('public/'.$publicUser->PU_ProfilePicPath);
           }

           //write new filepath to database
           $publicUser->PU_ProfilePicPath = substr($path, 7);
           $publicUser->save();

           return response()->json([
              "Message"=>"User's profile picture is updated!",
           ],200);
       }
       else
       {
           return response()->json([
               "Message"=>"No picture found!",
           ],400);
       }
   }
}
