<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LAStaffController extends Controller
{
    /**
     * METHOD: GET
     * Return Local Authority Staff Account Info including Local Authority Info
     * @return array
     */
    public function getUserProfileInfo()
    {
        $id = Auth::id();

        $query = "SELECT U.email, U.User_FirstName, U.User_LastName, LA.LA_Acronym, LA.LA_FullName, LA.* ";
        $query = $query."FROM Users U INNER JOIN LAStaff LAS ON U.User_ID_PK = LAS.LAS_ID_PK ";
        $query = $query."INNER JOIN LocalAuthority LA ON LAS.LA_ID_FK = LA_ID_PK ";
        $query = $query."WHERE U.User_ID_PK = ? ";

        $profileData = DB::select($query,[$id]);

        return $profileData;
    }

    /**
     * METHOD: PUT
     * Change Local Authority Staff Account Password
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeUserPassword(ChangePasswordRequest $request)
    {
        $user = Auth::guard('api')->user();

        if(Hash::check($request->currentPassword, $user->password))
        {
            $user->password = Hash::make($request->newPassword);
            $user->api_token = null;
            $user->save();

            return response()->json([
                'Message' => 'Password successfully changed, user is logged out!'], 200);
        }
        else
        {
            return response()->json([
                'Message' => 'Invalid Current Password!'], 400);

        }
    }
}
