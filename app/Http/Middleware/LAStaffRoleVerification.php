<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Model\User\Role;

use Closure;

class LAStaffRoleVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $role = Role::find($user->Ro_ID_FK);
        $roleName = $role->Ro_Name;

        //Ro_ID_FK:2 represents Local Authority Staff
        if ($roleName == 'Local Authority Staff') {
            return $next($request);
        } else {
            return response()->json([
                'error' => 'Insufficient Permission',
                'role' => $roleName,
            ], 403);
        }
    }
}
