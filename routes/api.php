<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('hello', function () {
    return 'Hello World';
});

Route::get('autoLogin', 'Auth\LoginController@autoLogin');

Route::post('login/public-user', 'Auth\LoginController@authenticatePublicUser');
Route::post('login/local-authority-staff', 'Auth\LoginController@authenticateLAStaff');
Route::post('logout', 'Auth\LoginController@logout');

Route::post('register', 'Auth\Register\RegisterController@register');
Route::post('register/check-email', 'Auth\Register\RegisterController@validateEmail');


//With Route::group, you can only assign middleware(s), namespace(s) to multiple routes
Route::group(['middleware' => ['auth:api', 'public.user']], function () {

    Route::get('profile/public-user', 'User\PublicUserController@getUserProfileInfo');
    Route::post('profile/public-user/change-password', 'User\PublicUserController@changeUserPassword');
    Route::post('profile/public-user/change-avatar', 'User\PublicUserController@changeProfilePic');

    Route::get('local-authority/index', 'Organization\LocalAuthorityController@index');
    Route::get('facility-type/index', 'Complaint\FacilityTypeController@index');

    Route::post('complaint-ticket/submit', 'Complaint\ComplaintTicketController@store');
    Route::get('complaint-ticket/public-user/my-ticket', 'Complaint\ComplaintTicketController@retrievePublicUserTicket');
    Route::get('complaint-ticket/public-user/messages/{ticketid}', 'Complaint\MessagesController@getMessagesByTicketID');

    Route::get('subscribed-project', 'M_Project\MaintenanceProjectController@getSubscribedProject');
    Route::get('subscribed-project/progress/{projectid}', 'M_Project\ProjectProgressController@getProgress');

    Route::get('subscribed-project/feedback/{projectid}', 'M_Project\ProjectFeedbackController@getFeedbackListForPublicUser');
    Route::post('subscribed-project/feedback/submit', 'M_Project\ProjectFeedbackController@storeFeedback');
    Route::put('subscribed-project/feedback/submit/{feedbackid}', 'M_Project\ProjectFeedbackController@editFeedback');

    Route::get('other-project/{laID}', 'M_Project\MaintenanceProjectController@getOtherProject');

    Route::get('other-project/subscribe/{projectID}', 'M_Project\MProjectSubscriptionController@publicUserSubscribeProject');

});

Route::group(['middleware' => ['auth:api', 'la.staff']], function () {

    Route::get('profile/local-authority-staff', 'User\LAStaffController@getUserProfileInfo');
    Route::post('profile/local-authority-staff/change-password', 'User\LAStaffController@changeUserPassword');

    Route::get('complaint-ticket/pending-ticket', 'Complaint\ComplaintTicketController@retrieveLocalAuthorityPendingTicket');
    Route::get('complaint-ticket/approved-ticket', 'Complaint\ComplaintTicketController@retrieveLocalAuthorityApprovedTicket');
    Route::get('complaint-ticket/rejected-ticket', 'Complaint\ComplaintTicketController@retrieveLocalAuthorityRejectedTicket');

    Route::get('complaint-ticket/approve-ticket/{id}', 'Complaint\ComplaintTicketController@approveTicket');
    Route::get('complaint-ticket/reject-ticket/{id}', 'Complaint\ComplaintTicketController@rejectTicket');

    Route::get('complaint-ticket/local-authority/messages/{ticketid}', 'Complaint\MessagesController@getMessagesByTicketID');
    Route::post('complaint-ticket/local-authority/messages/submit', 'Complaint\MessagesController@store');

    Route::get('maintenance-project/public-request-project', 'M_Project\MaintenanceProjectController@getPublicRequestProject');
    Route::get('maintenance-project/local-authority-project', 'M_Project\MaintenanceProjectController@getLocalAuthorityStaffProject');
    Route::put('maintenance-project/update-detail/{id}', 'M_Project\MaintenanceProjectController@updateMaintenanceProject');
    Route::post('maintenance-project/submit', 'M_Project\MaintenanceProjectController@createMaintenanceProject');

    Route::get('maintenance-project/progress/{projectid}', 'M_Project\ProjectProgressController@getProgress');
    Route::post('maintenance-project/progress/submit', 'M_Project\ProjectProgressController@store');

    Route::get('maintenance-project/feedback/{projectid}', 'M_Project\ProjectFeedbackController@getFeedbackListForLAStaff');

    Route::get('project-status/index', 'M_Project\ProjectStatusController@index');

});


