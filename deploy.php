<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'fyp_everything_php');

// Project repository
set('repository', 'git@bitbucket.org:1NV1NC1BLE/fyp_everything_php.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false); 

set('ssh_multiplexing', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('104.248.155.240')
    ->user('deployer')
    ->identityFile('C:/Users/junth/.ssh/deployerkey')
    ->set('deploy_path', '/var/www/html/fyp_everything_php');    
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');

