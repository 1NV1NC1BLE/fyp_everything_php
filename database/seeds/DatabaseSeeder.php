<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocalAuthorityTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(FacilityTypeTableSeeder::class);
        $this->call(TicketStatusTableSeeder::class);
        $this->call(ComplaintTicketTableSeeder::class);
        $this->call(MessageTableSeeder::class);
        $this->call(ProjectStatusTableSeeder::class);
        $this->call(MaintenanceProjectTableSeeder::class);
        $this->call(ProjectProgressTableSeeder::class);
        $this->call(ProjectFeedbackTableSeeder::class);
        $this->call(ProjectSubscriptionTableSeeder::class);
    }
}
