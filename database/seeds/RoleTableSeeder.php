<?php

use Illuminate\Database\Seeder;
use App\Model\User\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create([

            'Ro_Name' => 'Public User',
        ]);

        Role::create([

            'Ro_Name' => 'Local Authority Staff',
        ]);
    }
}
