<?php

use Illuminate\Database\Seeder;
use App\Model\Complaint\FacilityType;

class FacilityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FacilityType::truncate();

        $facilityTypeList = [
            [
                "name"   => "Others",
            ],
            [
                "name"   => "Building",
            ],
            [
                "name"   => "Bus Stop",
            ],
            [
                "name"   => "BillBoard",
            ],
            [
                "name"   => "Drain",
            ],
            [
                "name"   => "Road",
            ],
            [
                "name"   => "Road Sign",
            ],
            [
                "name"   => "Traffic Light",
            ],
            [
                "name"   => "Street Light",
            ],
            [
                "name"   => "Parking Ticket Machine",
            ],

        ];

        for ($i = 0; $i<10;$i++){
            FacilityType::create([
                'FT_TypeName'=>$facilityTypeList[$i]["name"],
            ]);
        }
    }
}
