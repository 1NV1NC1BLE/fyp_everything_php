<?php

use Illuminate\Database\Seeder;
use App\Model\Complaint\ComplaintTicket;

class ComplaintTicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Singapore');
        ComplaintTicket::truncate();

        $faker = \Faker\Factory::create();

        //Complaint Ticket in pending status
        ComplaintTicket::create([

            'CT_ID_PK' => 1805280205403282,
            'CT_Title' => "Damaged road sign causing confusion",
            'CT_Description' => "The road signboard in alan Radin, Sri Petaling has been left unrepaired for several months. 
            This may create confusion and endanger the motorists and pedestrians",
            'FT_ID_FK' => 7, //sign board (facility type)
            'PU_ID_FK' => 1,
            'TS_ID_FK' => 1, //pending
            'CT_Address' => "Jalan Radin Anum 2, Bandar Baru Seri Petaling, Sri Petaling, 57000 Kuala Lumpur",
            'CT_Photo1Path' => 'ticket-photos/Q1NSJrNeZHzJUZNH4GQZlblIJPg52SmcT24YoiEy.jpeg',
            'LA_ID_FK' => 1, //ticket is reported to DBKL (id:1)
            'LAS_ID_FK' => 11, //staff from DBKL (id:11)

        ]);

        //Complaint Ticket that is in processing status
        ComplaintTicket::create([

            'CT_ID_PK' => 1805280205312856,
            'CT_Title' => "Poor timing of traffic light",
            'CT_Description' => "Poor timing of traffic light in Taman Tun Dr Ismail, KL causing traffic congestion especially during morning peak hours. It will take more than 10 minutes to drive out of Jalan Datuk Sulaiman.",
            'FT_ID_FK' => 8, //traffic light (facility type)
            'PU_ID_FK' => 1,
            'TS_ID_FK' => 1, //pending
            'CT_Address' => "",
            'CT_XCor' => 3.1254776,
            'CT_YCor' => 101.6410891,
            'CT_Photo1Path' => 'ticket-photos/p3kEcorszcRA33LaI8QBgKUJWRtq3S6RaTwdzeTP.jpeg',
            'LA_ID_FK' => 1, //ticket is reported to DBKL (id:1)
            'LAS_ID_FK' => 11, //staff from DBKL (id:11)

        ]);

        //Complaint Ticket that is in approved status
        ComplaintTicket::create([

            'CT_ID_PK' => 1805281005116847,
            'CT_Title' => "Road Pothole in Taman OUG area",
            'CT_Description' => "The road Jalan Hujan Emas 5 is uneven and not safe for road user, especially for motorcyclists.",
            'FT_ID_FK' => 6, //road (facility type)
            'PU_ID_FK' => 1,
            'TS_ID_FK' => 5, //approved
            'CT_Address' => "Jalan Hujan Emas 5, Taman Overseas Union, 58200 Kuala Lumpur",
            'CT_Photo1Path' => 'ticket-photos/0QbdxqQW8jV8QwC2O4vdzJZ3VkzB1rndBsf0eC2x.jpeg',
            'CT_Photo2Path' => 'ticket-photos/6bGRX2BZVesdTCt5B1EtnvQuzpcjdTJAKzXB5kVx.jpeg',
            'CT_Photo3Path' => 'ticket-photos/msF3XwEn75a6ft0ZSQMJ3wqHkuyqNJDZT77BRbum.jpeg',
            'LA_ID_FK' => 1, //ticket is reported to DBKL (id:1)
            'LAS_ID_FK' => 11, //staff from DBKL (id:11)
            //'MP_ID_FK' => 1456324134,

        ]);

        //Complaint Ticket that is in rejected status
        ComplaintTicket::create([

            'CT_ID_PK' => 1805291005004339,
            'CT_Title' => "Unpleasant smell from drainage system",
            'CT_Description' => "Coffeeshop was suspected discharging wastewater directly into a drain that was also strewn with rubbish and food waste, causing drain blockage and unpleasant smell.",
            'FT_ID_FK' => 5, //drain (facility type)
            'PU_ID_FK' => 1,
            'TS_ID_FK' => 4, //rejected
            'CT_Address' => "Jalan Singa C 20/C, Seksyen 20, 40300 Shah Alam, Selangor",
            'CT_Photo1Path' => '',
            'LA_ID_FK' => 1, //ticket is reported to DBKL (id:1)
            'LAS_ID_FK' => 11, //staff from DBKL (id:11)

        ]);


        //For Other Project (User that is not subscribed to it)
        ComplaintTicket::create([

            'CT_ID_PK' => 1805291005004400,
            'CT_Title' => "Faulty Street Lighting",
            'CT_Description' => "Street Lighting in Jalan PJS 2/3 has not been working for a week.",
            'FT_ID_FK' => 9, //street light (facility type)
            'PU_ID_FK' => 3,
            'TS_ID_FK' => 5, //approved
            'CT_Address' => "Jalan PJS 2/3, Taman Medan, 46000 Petaling Jaya, Selangor",
            'CT_Photo1Path' => 'ticket-photos/0voQUTunEqtthM38uQL053X9WOdFV2zmDejic7AM.jpeg',
            'LA_ID_FK' => 4, //ticket is reported to MBPJ (id:4)
            'LAS_ID_FK' => 14, //staff from  (id:4)
            //'MP_ID_FK' => 1456324100,

        ]);

        ComplaintTicket::create([

            'CT_ID_PK' => 1805291005004500,
            'CT_Title' => "Bus Stop station at Old Klang Road",
            'CT_Description' => "The bus stop timetable LED display is not working properly.",
            'FT_ID_FK' => 9, //street light (facility type)
            'PU_ID_FK' => 3,
            'TS_ID_FK' => 5, //approved
            'CT_Address' => "Batu 3/4, 1, 1,2, Jalan Klang Lama, Taman United, 58000 Kuala Lumpur",
            'CT_Photo1Path' => 'ticket-photos/IY4CtG1IxUXVAYGTaNfYV0EzT7SEcdN8i36k6Glh.jpeg',
            'LA_ID_FK' => 1, //ticket is reported to DBKL (id:1)
            'LAS_ID_FK' => 11, //staff from  (id:4)
            //'MP_ID_FK' => 1456324200,

        ]);
    }

}
