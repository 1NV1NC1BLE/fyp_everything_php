<?php

use Illuminate\Database\Seeder;
use App\Model\M_Project\ProjectFeedback;

class ProjectFeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectFeedback::truncate();

        ProjectFeedback::create([

            'PF_Title' => 'Well Done',
            'PF_Message' => 'The local authority did a good job for fixing the road pothole, the duration of the project could be shorten',
            'PF_Rating' => 4,
            'PF_DateCreated' => '2018-06-27 04:18:51',
            'PF_DateUpdated' => '2018-06-27 04:18:51',
            'PU_ID_FK' => 3,
            'MP_ID_FK' => 145632413

        ]);


        /*$faker = \Faker\Factory::create();

        //Create a feedback for each of the following maintenance project
        $m_projectID = [1456324134, 7524635356, 4624146256, 2462247234];

        //for public user(id): 1
        for($i = 0; $i < 3; $i++){

            ProjectFeedback::create([

            'PF_Title' => $faker->text(50),
            'PF_Message' => $faker->text(150),
            'PF_Rating' => $faker->numberBetween(1,5),
            'PF_DateCreated' => $faker->dateTime,
            'PF_DateUpdated' => $faker->dateTime,
            'PU_ID_FK' => 1,
            'MP_ID_FK' => $m_projectID[$i]

            ]);
        }

        //for public user(id): 3
        for($i = 0; $i < 3; $i++){

            ProjectFeedback::create([

                'PF_Title' => $faker->text(50),
                'PF_Message' => $faker->text(150),
                'PF_Rating' => $faker->numberBetween(1,5),
                'PF_DateCreated' => $faker->dateTime,
                'PF_DateUpdated' => $faker->dateTime,
                'PU_ID_FK' => 3,
                'MP_ID_FK' => $m_projectID[$i]

            ]);
        }*/

    }
}
