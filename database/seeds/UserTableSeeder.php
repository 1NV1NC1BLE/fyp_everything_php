<?php

use Illuminate\Database\Seeder;
use App\Model\User\User;
use App\Model\User\PublicUser;
use App\Model\User\LAStaff;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //dump table records
        User::truncate();
        PublicUser::truncate();
        LAStaff::truncate();

        $faker = Faker\Factory::create();

        //Public User record 1
        User::create([
            'User_ID_PK' => 1,
            'email' => 'harold@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Harold',
            'User_LastName' => 'Stevens',
            'Ro_ID_FK' => 1,
        ]);

        PublicUser::create([
            'PU_ID_PK' => 1,
            'PU_DOB' => $faker->date(),
            'PU_ContactNo' => '0124567389',
            'PU_ProfilePicPath' => 'profile-pic/7A1ROtlxsL8rw3F5tm5wGw6u8rBjpNdctkWyzMRD.jpeg',
        ]);

        //Public User record 2
        User::create([
            'User_ID_PK' => 3,
            'email' => 'mitchell@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Mitchell',
            'User_LastName' => 'Robin',
            'Ro_ID_FK' => 1,
        ]);

        PublicUser::create([
            'PU_ID_PK' => 3,
            'PU_DOB' => $faker->date(),
            'PU_ContactNo' => '0199023451',
            'PU_ProfilePicPath' => '',
        ]);

        ///////////////////////////////////////////////////////////////

        //LocalAuthority Staff user record (id 11~20))

        User::create([
            'User_ID_PK' => 11,
            'email' => 'kiley@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Christian',
            'User_LastName' => 'Kiley',
            'Ro_ID_FK' => 2,
        ]);

        LAStaff::create([
            'LAS_ID_PK' => 11,
            'LA_ID_FK' => 1, //Works under DBKL (id:1)
        ]);

        //------------------------------------------------//
        User::create([
            'User_ID_PK' => 12,
            'email' => 'johnny@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Johnny',
            'User_LastName' => 'Lee',
            'Ro_ID_FK' => 2,
        ]);

        LAStaff::create([
            'LAS_ID_PK' => 12,
            'LA_ID_FK' => 5, //Works under MPSJ (id:2)
        ]);

        //------------------------------------------------//
        User::create([
            'User_ID_PK' => 13,
            'email' => 'mike@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Mike',
            'User_LastName' => 'Frank',
            'Ro_ID_FK' => 2,
        ]);

        LAStaff::create([
            'LAS_ID_PK' => 13,
            'LA_ID_FK' => 5, //Works under MPK (id:3)
        ]);

        //------------------------------------------------//
        User::create([
            'User_ID_PK' => 14,
            'email' => 'john@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'John',
            'User_LastName' => 'Fred',
            'Ro_ID_FK' => 2,
        ]);


        LAStaff::create([
            'LAS_ID_PK' => 14,
            'LA_ID_FK' => 4, //Works under MBPJ (id:4)
        ]);

        //------------------------------------------------//
        User::create([
            'User_ID_PK' => 15,
            'email' => 'kent@mail.com',
            'password' => Hash::make('123456'),
            'User_FirstName' => 'Kent',
            'User_LastName' => 'Ron',
            'Ro_ID_FK' => 2,
        ]);

        LAStaff::create([
            'LAS_ID_PK' => 15,
            'LA_ID_FK' => 5, //Works under MBSA (id:5)
        ]);

        /*//Seed PublicUser table
        for ($i = 3; $i <= 6; $i++) {

            //for every odd index, we create a public user record (1)
            //for every even index, we create a local authority record (2)

            $roleID = ($i % 2 != 0)?1:2;

            User::create([
                'User_ID_PK' => $i,
                'email' => $faker->email,
                'password' => $seedPass,
                'User_FirstName' => $faker->name,
                'User_LastName' => $faker->name,
                'Ro_ID_FK' => $roleID,
            ]);

            if($roleID == 1){
                PublicUser::create([
                    'PU_ID_PK' => $i,
                    'PU_DOB' => $faker->date(),
                    'PU_ContactNo' => $faker->phoneNumber,
                    'PU_ProfilePicPath' => $faker->text(255),
                ]);
            }
            elseif($roleID == 2){
                LAStaff::create([
                    'LAS_ID_PK' => $i,
                    'LA_ID_FK' => $i -1,
                ]);
            }
        }*/

    }
}
