<?php

use Illuminate\Database\Seeder;
use App\Model\M_Project\ProjectStatus;

class ProjectStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectStatus::truncate();

        ProjectStatus::create([
           'PS_Status' => 'Planning',

        ]);
        ProjectStatus::create([
            'PS_Status' => 'Implementing',

        ]);
        ProjectStatus::create([
            'PS_Status' => 'Completed',

        ]);

    }
}
