<?php

use Illuminate\Database\Seeder;
use App\Model\M_Project\MProjectSubscription;

class ProjectSubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Singapore');
        MProjectSubscription::truncate();

        MProjectSubscription::create([
            'PU_ID_FK' => 1,
            'MP_ID_FK' => 145632413,
        ]);
    }
}
