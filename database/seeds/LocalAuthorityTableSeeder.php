<?php

use Illuminate\Database\Seeder;
use App\Model\Organization\LocalAuthority;

class LocalAuthorityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        LocalAuthority::truncate();

        $faker = Faker\Factory::create();

        LocalAuthority::create([

            'LA_FullName' => 'Dewan Bandaraya Kuala Lumpur',
            'LA_Acronym' => 'DBKL',
            'LA_Address' => ' Kuala Lumpur City Centre, 50100 Kuala Lumpur, Federal Territory of Kuala Lumpur',
            'LA_Website' => 'http://www.dbkl.gov.my/',
            'LA_Email' => 'callcentre@dbkl.gov.my',
            'LA_ContactNo' => '1800-88-3255',
            'LA_LogoPicPath' => 'profile-pic/local-authority/kpfsBDKO2fXN8lMesPBwstzUw8WesTexjs1vXSuC.png'

        ]);

        LocalAuthority::create([

            'LA_FullName' => 'Majlis Perbandaran Subang Jaya',
            'LA_Acronym' => 'MPSJ',
            'LA_Address' => 'Persiaran Perpaduan, Usj 5, 47610 Subang Jaya, Selangor',
            'LA_Website' => 'http://www.mpsj.gov.my/',
            'LA_Email' => 'callcentre@mpsj.gov.my',
            'LA_ContactNo' => '1800-88-1234',
            'LA_LogoPicPath' => 'profile-pic/local-authority/D42WTOZRpKoDrMIaTgj1gfdBo2dEiosQOYbBFYvq.png'

        ]);

        LocalAuthority::create([

            'LA_FullName' => 'Majlis Perbandaran Klang',
            'LA_Acronym' => 'MPK',
            'LA_Address' => ' Kawasan 1, Klang, Selangor',
            'LA_Website' => 'http://www.mpk.gov.my/',
            'LA_Email' => 'callcentre@mpk.gov.my',
            'LA_ContactNo' => '1800-88-1598',
            'LA_LogoPicPath' => 'profile-pic/local-authority/ypSdEKu89UP4q1fizaQQxKMbWK90uXt05A6stWjU.jpeg'

        ]);

        LocalAuthority::create([

            'LA_FullName' => 'Majlis Bandaraya Petaling Jaya',
            'LA_Acronym' => 'MBPJ',
            'LA_Address' => 'No. 1, Jalan Yong Shook Lin, Seksyen 7, 46675 Petaling Jaya, Selangor',
            'LA_Website' => 'http://www.mbpj.gov.my/',
            'LA_Email' => 'callcentre@mpbj.gov.my',
            'LA_ContactNo' => '1800-88-7456',
            'LA_LogoPicPath' => 'profile-pic/local-authority/E6o9H4QIOGOmvCxDs74EthRo4SdPMjz4VbXzdyAV.png'

        ]);

        LocalAuthority::create([

            'LA_FullName' => 'Majlis Bandaraya Shah Alam',
            'LA_Acronym' => 'MBSA',
            'LA_Address' => 'Pesiaran Perbandaran, Seksyen 14, 40000 Shah Alam, Selangor',
            'LA_Website' => 'http://www.mbsa.gov.my/',
            'LA_Email' => 'callcentre@mbsa.gov.my',
            'LA_ContactNo' => '1800-88-8542',
            'LA_LogoPicPath' => 'profile-pic/local-authority/GWl07K6dsttSq3xZJMnahNUv4DP9xZFmovuufBNK.png'

        ]);

        /*for ($i = 0; $i < 5; $i++) {
            LocalAuthority::create([

                'LA_FullName' => $localAuthority[$i]["name"],
                'LA_Acronym' => $localAuthority[$i]["acronym"],
                'LA_Address' => $faker->address,
                'LA_Website' => $faker->text,
                'LA_Email' => $faker->email,
                'LA_ContactNo' => $faker->phoneNumber,
                'LA_LogoPicPath' => $localAuthority[$i]["profilepic"]

            ]);
        }*/
    }
}
