<?php

use Illuminate\Database\Seeder;
use App\Model\M_Project\MaintenanceProject;

class MaintenanceProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Singapore');

        MaintenanceProject::truncate();

        $faker = \Faker\Factory::create();

        $m_projectID = [145632413, 145632410, 145632420, 145632430];

        MaintenanceProject::create([
            'MP_ID_PK' => 145632413,
            'MP_Title' => "Road Pothole in Taman OUG area",
            'MP_Description' => "Road Maintenance Project, road pavement will be carried out",
            'MP_StartDate' => "2018/07/03",
            'MP_EndDate' => "2018/07/05",
            'MP_EstimateBudget' => 2500,
            'PS_ID_FK' => 3,
            'LAS_ID_FK' => 11,
            'CT_ID_FK' => 1805281005116847

        ]);


        //Other project that is currently not subscribed by the user (id:1)
        MaintenanceProject::create([
            'MP_ID_PK' => 145632410,
            'MP_Title' => "Faulty Street Lighting",
            'MP_Description' => "TNB (Tenaga Nasional Berhad) is responsible to take action and repair the reported case.",
            'MP_StartDate' => "2018/07/04",
            'MP_EndDate' => "2018/07/06",
            'MP_EstimateBudget' => 13000,
            'PS_ID_FK' => 1,
            'LAS_ID_FK' => 14,
            'CT_ID_FK' => 1805291005004400

        ]);

        MaintenanceProject::create([
            'MP_ID_PK' => 145632420,
            'MP_Title' => "Bus stop LED Display Maintenance Task",
            'MP_Description' => "LED display replacement will be carried out.",
            'MP_StartDate' => "2018/07/04",
            'MP_EndDate' => "2018/07/06",
            'MP_EstimateBudget' => 13000,
            'PS_ID_FK' => 1,
            'LAS_ID_FK' => 11,
            'CT_ID_FK' => 1805291005004500

        ]);

    }
}
