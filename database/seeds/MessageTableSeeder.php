<?php

use Illuminate\Database\Seeder;
use App\Model\Complaint\Messages;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Singapore');
        Messages::truncate();

        $faker = \Faker\Factory::create();


        //Ticket ID: 1805280205403282 (pending status)
        Messages::create([
            'Mes_Title' => "Ticket Received!",
            'Mes_Message' => "Your ticket will be reviewed by the local authority staff soon.",
            'CT_ID_FK' => 1805280205403282,
        ]);

        //Ticket ID: 1805280205312856 (pending status)

        Messages::create([
            'Mes_Title' => "Ticket Received!",
            'Mes_Message' => "Your ticket will be reviewed by the local authority staff soon.",
            'CT_ID_FK' => 1805280205312856,
        ]);

        //Ticket ID: 1805281005116847 (approved)

        Messages::create([
            'Mes_Title' => "Ticket Received!",
            'Mes_Message' => "Your ticket will be reviewed by the local authority staff soon.",
            'CT_ID_FK' => 1805281005116847,
        ]);

        Messages::create([
            'Mes_Title' => "Ticket Approved!",
            'Mes_Message' => "Your ticket has been reviewed and accepted!",
            'CT_ID_FK' => 1805281005116847,
        ]);

        Messages::create([
            'Mes_Title' => "A Maintenance Project is initiated!",
            'Mes_Message' => "A maintenance project is regarding to this ticket has been created, you have been automatically subscribed to this project, please check it out at Subscribed Project section",
            'CT_ID_FK' => 1805281005116847,
        ]);

        //Ticket ID: 1805291005004339 (Rejected)

        Messages::create([
            'Mes_Title' => "Ticket Received!",
            'Mes_Message' => "Your ticket will be reviewed by the local authority staff soon.",
            'CT_ID_FK' => 1805291005004339,
        ]);


        Messages::create([
            'Mes_Title' => "Ticket Rejected!",
            'Mes_Message' => "The facility is not under the management of DBKL, please consider lodge the ticket to MBSA",
            'CT_ID_FK' => 1805291005004339,
        ]);
    }
}
