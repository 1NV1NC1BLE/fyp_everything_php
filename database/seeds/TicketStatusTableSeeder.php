<?php

use Illuminate\Database\Seeder;
use App\Model\TicketStatus;

class TicketStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketStatus::truncate();

        $ticketStatus = ['Pending', 'Processing', 'Approved', 'Rejected', 'Project Created'];

        for ($i = 0; $i < 5; $i++) {
            TicketStatus::create([

                'TS_Status' => $ticketStatus[$i],
            ]);
        }

    }
}
