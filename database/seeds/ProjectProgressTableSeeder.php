<?php

use Illuminate\Database\Seeder;
use App\Model\M_Project\ProjectProgress;

class ProjectProgressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Singapore');
        ProjectProgress::truncate();

        $faker = \Faker\Factory::create();

        //Maintenance Project ID: 1456324134
        ProjectProgress::create([

            'PP_Title' => 'Maintenance Project Initiated',
            'PP_Message' => 'Officers would be dispatched to inspect the site and engage a contractor to carry out the repair.',
            'MP_ID_FK' => 145632413,
            'PP_DateCreated' => '2018-06-23 05:18:51',
        ]);
        ProjectProgress::create([

            'PP_Title' => 'Maintenance Project is carried out',
            'PP_Message' => 'Road resurface and repairing is carrying out by the contractors.',
            'MP_ID_FK' => 145632413,
            'PP_DateCreated' => '2018-06-26 05:18:51',
        ]);

        ProjectProgress::create([

            'PP_Title' => 'Maintenance Task is done',
            'PP_Message' => 'The maintenance activities is completed on time.',
            'MP_ID_FK' => 145632413,
            'PP_DateCreated' => '2018-06-27 05:18:51',
        ]);

        //Maintenance Project ID: 1456324100
        ProjectProgress::create([

            'PP_Title' => 'Maintenance Project Initiated',
            'PP_Message' => 'The maintenance project is approved and currently in planning phase',
            'MP_ID_FK' => 145632410,
            'PP_DateCreated' => '2018-06-27 05:18:51',
        ]);

        //Maintenance Project ID: 1456324200
        ProjectProgress::create([

            'PP_Title' => 'Maintenance Project Initiated',
            'PP_Message' => 'The maintenance project is approved and currently in planning phase',
            'MP_ID_FK' => 145632420,
            'PP_DateCreated' => '2018-06-27 14:18:51',
        ]);
    }
}
