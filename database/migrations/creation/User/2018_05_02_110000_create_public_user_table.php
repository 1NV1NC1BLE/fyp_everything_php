<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicUserTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('PublicUser', function (Blueprint $table) {
            $table->increments('PU_ID_PK')->unique();
            $table->date('PU_DOB');
            $table->string('PU_ContactNo', 30);
            $table->string('PU_ProfilePicPath', 255)->nullable();
          });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PublicUser');
    }
}
