<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->increments('User_ID_PK')->unique();
            $table->string('email',128)->unique();
            $table->string('password', 255);
            $table->string('User_FirstName', 100);
            $table->string('User_LastName', 100);
            $table->timestamp('User_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('User_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('api_token',60)->unique()->nullable();
            $table->rememberToken();
            $table->integer('Ro_ID_FK')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Users');
    }
}
