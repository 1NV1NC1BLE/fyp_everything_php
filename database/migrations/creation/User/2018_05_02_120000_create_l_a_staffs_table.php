<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLAStaffsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('LAStaff', function (Blueprint $table) {
            $table->increments('LAS_ID_PK')->unique();
            $table->integer('LA_ID_FK');
        });


    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LAStaff');
    }
}
