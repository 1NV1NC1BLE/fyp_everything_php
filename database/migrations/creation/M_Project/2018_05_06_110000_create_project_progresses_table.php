<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectProgressesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ProjectProgress', function (Blueprint $table) {
            $table->increments('PP_ID_PK')->unique();
            $table->string('PP_Title',255);
            $table->text('PP_Message');
            $table->timestamp('PP_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('PP_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->bigInteger('MP_ID_FK');
        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ProjectProgress');
    }
}
