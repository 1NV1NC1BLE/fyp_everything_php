<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ProjectFeedback', function (Blueprint $table) {
            $table->increments('PF_ID_PK')->unique();
            $table->string('PF_Title', 255);
            $table->text('PF_Message');
            $table->unsignedTinyInteger('PF_Rating'); //1-5 score
            $table->timestamp('PF_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('PF_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('PU_ID_FK');
            $table->bigInteger('MP_ID_FK');

        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ProjectFeedback');
    }
}
