    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceProjectsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('MaintenanceProject', function (Blueprint $table) {
            $table->bigIncrements('MP_ID_PK')->unique();
            $table->string('MP_Title',255);
            $table->text('MP_Description');
            $table->date('MP_StartDate');
            $table->date('MP_EndDate');
            $table->decimal('MP_EstimateBudget',12,2);
            $table->timestamp('MP_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('MP_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('PS_ID_FK');
            $table->integer('LAS_ID_FK')->nullable();
            $table->bigInteger('CT_ID_FK')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MaintenanceProject');
    }
}
