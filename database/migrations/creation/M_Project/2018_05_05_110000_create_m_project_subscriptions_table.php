<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMProjectSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('MProjectSubscription', function (Blueprint $table) {
            $table->integer('PU_ID_FK');
            $table->bigInteger('MP_ID_FK');
            $table->timestamp('MPS_DateSubscribe')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->primary(['PU_ID_FK','MP_ID_FK']);
        });

    }

    /**
     * Reverse the migrations
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MProjectSubscription');
    }
}
