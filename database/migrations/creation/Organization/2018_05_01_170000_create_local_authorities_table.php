<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */

    public function up()
    {
        Schema::create('LocalAuthority', function (Blueprint $table) {
            $table->increments('LA_ID_PK')->unique();
            $table->string('LA_FullName',255);
            $table->string('LA_Acronym',20);
            $table->string('LA_Address',255);
            $table->string('LA_Website', 255);
            $table->string('LA_Email', 255);
            $table->string('LA_ContactNo', 30);
            $table->string('LA_LogoPicPath', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LocalAuthority');
    }
}
