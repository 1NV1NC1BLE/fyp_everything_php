<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('Messages', function (Blueprint $table) {
            $table->increments('Mes_ID_PK')->unique();
            $table->string('Mes_Title', 255);
            $table->text('Mes_Message');
            $table->timestamp('Mes_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('Mes_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->bigInteger('CT_ID_FK')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Messages');
    }
}
