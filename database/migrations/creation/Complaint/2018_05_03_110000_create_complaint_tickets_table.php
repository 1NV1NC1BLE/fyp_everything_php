<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintTicketsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ComplaintTicket', function (Blueprint $table) {
            $table->bigIncrements('CT_ID_PK')->unique();
            $table->string('CT_Title', 255);
            $table->text('CT_Description');
            $table->string('CT_Address', 255)->nullable();
            $table->double('CT_XCor')->nullable();
            $table->double('CT_YCor')->nullable();
            $table->string('CT_Photo1Path', 255)->nullable();
            $table->string('CT_Photo2Path', 255)->nullable();
            $table->string('CT_Photo3Path', 255)->nullable();
            $table->timestamp('CT_DateCreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('CT_DateUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('TS_ID_FK')->nullable();
            $table->integer('PU_ID_FK')->nullable();
            $table->integer('FT_ID_FK')->nullable();
            $table->integer('LA_ID_FK')->nullable();
            $table->integer('LAS_ID_FK')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ComplaintTicket');
    }
}
